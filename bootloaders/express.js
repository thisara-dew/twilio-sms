const required = require('esm')(module);

const app = required('../app');

const port = process.env.PORT || 3000;

app.listen(port, () => {
    const { name: appName, version: appVersion } = require('../package.json');
    console.log({ appName, appVersion, port });
});
