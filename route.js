import controllers from './controllers';

const express = require('express');
function init() {
    const router = express.Router();
    router.get('/', (req, res) => {
        res.status(200).json({
            health: '100%'
        });
    });
    router.post(
        '/send-sms',
        controllers.sendSms
    );
    return router;
}

export default { init };