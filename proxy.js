async function sendSms(data) {

    const accountSid = config.TWILIO_ACCOUNT_SID;
    const authToken = config.TWILIO_AUTH_TOKEN;
    const client = require('twilio')(accountSid, authToken);
    const from = config.TWOLIO_FROM;
    
    const {
        message,
        to
    } = data;
    client.messages
        .create({
            body: message,
            from,
            to
        })
        .then(message => {
            return message;
        });

}

export default {
    sendSms
}