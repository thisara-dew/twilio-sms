import proxy from "./proxy"

async function sendSms (req, res) {
    try {
        const {
            body: {
                message,
                to
            }
        } = req;
        await proxy.sendSms({
            message,
            to
        });
        res.status(201).send({
            message: 'Sms sent successfully!',
            data: result
        })
    } catch (error) {
        console.log(error);
        res.status(500).send(error)
    }
}

export default {
    sendSms
}