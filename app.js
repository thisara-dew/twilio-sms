#!/usr/bin/node
import express from "express";
import BodyParser from "body-parser";
import dotenv from "dotenv";
import route from "./route";

global.config = dotenv.config().parsed;
const app = express();
app.use(express.json());
app.use(BodyParser.json());
app.use(express.urlencoded({ extended: true }));

app.use('/', route.init());

module.exports = app;
